package pl.poznan.put.spio.web;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import io.restassured.module.mockmvc.response.MockMvcResponse;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.poznan.put.spio.controller.GaDeRyPoLuKiController;
import pl.poznan.put.spio.controller.model.ErrorHandler;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class GaDeRyPoLukiWebTest {

    @Autowired
    private GaDeRyPoLuKiController controller;

    @BeforeEach
    public void setUp() {
        final MockMvc mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .setControllerAdvice(new ErrorHandler())
                .build();

        RestAssuredMockMvc.mockMvc(mockMvc);
    }

    @Test
    public void shouldTranslateWithDefaultKey() {
        final MockMvcResponse response = given()
                .when()
                .get("/gaderypoluki/{origin}", "kot")
                .then()
                .assertThat()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .response();

        assertThat(JsonPath.from(response.asString()).get("translated").toString()).isEqualTo("ipt");
    }

    @Test
    public void shouldGetNotFoundOnInternalException() {
        given()
                .when()
                .get("/gaderypoluki/{origin}", "wąż")
                .then()
                .assertThat()
                .statusCode(HttpStatus.NOT_FOUND.value())
                .extract()
                .response();
    }

    @Test
    public void shouldTranslateWithIgnoreCase() {
        final MockMvcResponse response = given()
                .when()
                .get("/gaderypoluki/{origin}/ignorecase", "kOt")
                .then()
                .assertThat()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .response();

        assertThat(JsonPath.from(response.asString()).get("translated").toString()).isEqualTo("ipt");
    }

    @Test
    public void shouldGetNotFoundOnInternalExceptionForIgnoreCase() {
        given()
                .when()
                .get("/gaderypoluki/{origin}/ignorecase", "Wąż")
                .then()
                .assertThat()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void shouldTranslateWithSpecificKey() {
        final MockMvcResponse response = given()
                .when()
                .get("/gaderypoluki/{msg}/with/{key}", "kot", "kaceminutowy")
                .then()
                .assertThat()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .response();

        assertThat(JsonPath.from(response.asString()).get("translated").toString()).isEqualTo("ato");
    }

    /**
     * This translation will fail, because translation key has 13 characters.
     */
    @Test
    public void shouldGetBadRequestOnInternalException() {
        given()
                .when()
                .get("/gaderypoluki/{msg}/with/{key}", "kot", "akaceminutowy")
                .then()
                .assertThat()
                .statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void shouldTranslateWithSpecificKeyWithIgnoreCase() {
        final MockMvcResponse response = given()
                .when()
                .get("/gaderypoluki/{msg}/with/{key}/ignorecase", "OpilUbaD", "polibuda")
                .then()
                .assertThat()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .response();

        assertThat(JsonPath.from(response.asString()).get("translated").toString()).isEqualTo("polibuda");
    }

    @Test
    public void shouldGetBadRequestOnInternalExceptionForIgnoreCase() {
        given()
                .when()
                .get("/gaderypoluki/{msg}/with/{key}", "kOt", "polibudak")
                .then()
                .assertThat()
                .statusCode(HttpStatus.BAD_REQUEST.value());
    }
}