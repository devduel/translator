package pl.poznan.put.spio.translator.control;

import org.springframework.stereotype.Service;
import pl.poznan.put.spio.translator.boundary.Translator;

@Service
public class CaesarTranslator implements Translator {

    private static final int DIFFERENCE_BETWEEN_CHARACTER_SIZES = 32;
    private static final int DEFAULT_OFFSET = 3;

    private static final char FIRST_ALPHABET_LETTER = 'a';

    @Override
    public String translate(String message) {
        return translate(message, DEFAULT_OFFSET);
    }

    private String translate(String message, int offset) {
        StringBuilder stringBuilder = new StringBuilder();

        for (char character : message.toCharArray()) {
            if (character != ' ') {
                int newCharacterPosition = getNewCharacterPosition(character, offset);
                char shiftedCharacter = (char) (FIRST_ALPHABET_LETTER + newCharacterPosition);

                stringBuilder.append(shiftedCharacter);
            } else {
                stringBuilder.append(character);
            }
        }

        return stringBuilder.toString();
    }

    private int getNewCharacterPosition(char character, int offset) {
        int newCharacterPosition = calculateNewCharacterPosition(character, offset);
        if (Character.isUpperCase(character)) {
            return newCharacterPosition - DIFFERENCE_BETWEEN_CHARACTER_SIZES;
        }

        return newCharacterPosition;
    }

    private int calculateNewCharacterPosition(char character, int offset) {
        char lowerCasedCharacter = Character.toLowerCase(character);
        int originalPosition = lowerCasedCharacter - FIRST_ALPHABET_LETTER;
        return (originalPosition + offset) % 26;
    }
}