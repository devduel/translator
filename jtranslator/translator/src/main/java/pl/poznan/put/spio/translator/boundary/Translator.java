package pl.poznan.put.spio.translator.boundary;

public interface Translator {

    String translate(String message);
}