package pl.poznan.put.spio.tdd;

import org.junit.jupiter.api.Test;
import pl.poznan.put.spio.translator.boundary.Translator;
import pl.poznan.put.spio.translator.control.CaesarTranslator;
import pl.poznan.put.spio.translator.control.GaDeRyPoLuKiTranslator;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TranslatorTest {

    private Translator translator;

    @Test
    void shouldTranslateUsingCaesarTranslator() {
        translator = new CaesarTranslator();

        final String message = "Ala ma kota";

        final String result = translator.translate(message);

        assertEquals("Dod pd nrwd", result);
    }

    @Test
    void shouldTranslateUsingGaderyPolukiTranslator() {
        translator = new GaDeRyPoLuKiTranslator();

        final String message = "LoK";

        final String result = translator.translate(message);

        assertEquals("UpI", result);
    }
}